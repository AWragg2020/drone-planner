import { GeneralHeaderComponentComponent } from './../general-header-component/general-header-component.component';
import { CommunicationServiceService } from './../_services/communication-service.service';
import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Draw from 'ol/interaction/Draw.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-map-component',
  templateUrl: './map-component.component.html',
  styleUrls: ['./map-component.component.css']
})
export class MapComponentComponent implements OnInit {
  
  _raster: TileLayer;
  _source: VectorSource;
  _vector: VectorLayer;
  _map: Map;
  _draw: Draw;
  private parentSubscription = new Subscription();

  constructor(private communcationService: CommunicationServiceService) { }

  ngOnInit() {

    // Subscription management
    this.getSaveRouteButtonClicked();
    this.getRouteMenuToMap();
    this.getNewRouteButtonClicked();

    // Map initialization
    this._raster = new TileLayer({
      source: new OSM()
    });

    this._source = new VectorSource();

    this._vector = new VectorLayer({
      source: this._source
    });

    this._map = new Map({
      layers: [this._raster, this._vector],
      target: 'map',
      view: new View({
        center: [-410000, 4925000],
        zoom: 14
      })
    });

    this._draw = new Draw({
      source: this._source,
      type: 'LineString'
    });

    this._draw.on('drawend', (event) => {
      this._map.removeInteraction(this._draw);
    });

    this._map.addInteraction(this._draw);
  }

  getSaveRouteButtonClicked(){
    const subscription = this.communcationService.listenToSaveRouteButton.subscribe(
      (event: any) => {

        // Sends the current route as coordinates to the menu
        const features = this._source.getFeatures();
        if(features.length >= 1) {
          const route = features[0];
          this.communcationService.setRouteMapToMenu(route);
        }
      }
    );

    this.parentSubscription.add(subscription);
  }

  getRouteMenuToMap(){
    const suscription = this.communcationService.getRouteMenuToMap().subscribe(
      (route: any) => {
        this._source.clear();
        this._source.addFeature(route);
      }
    );
    this.parentSubscription.add(suscription);
  }

  getNewRouteButtonClicked(){
    const subscription = this.communcationService.listenToNewRouteButton.subscribe(
      (event: any) => {

        // Add draw interaction for the map
        this._map.addInteraction(this._draw);

        // Clear all features on map
        this._source.clear();

      }
    );

    this.parentSubscription.add(subscription);

  }

}
