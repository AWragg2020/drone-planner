import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-general-header-component',
  templateUrl: './general-header-component.component.html',
  styleUrls: ['./general-header-component.component.css']
})
export class GeneralHeaderComponentComponent implements OnInit {

  @Input('text') _text: string = '';

  constructor() { }

  ngOnInit() {

  }

}
