import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralHeaderComponentComponent } from './general-header-component.component';

describe('GeneralHeaderComponentComponent', () => {
  let component: GeneralHeaderComponentComponent;
  let fixture: ComponentFixture<GeneralHeaderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralHeaderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralHeaderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
