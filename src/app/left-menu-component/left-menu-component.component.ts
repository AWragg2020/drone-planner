import { Subscription } from 'rxjs';
import { CommunicationServiceService } from './../_services/communication-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left-menu-component',
  templateUrl: './left-menu-component.component.html',
  styleUrls: ['./left-menu-component.component.css']
})
export class LeftMenuComponentComponent implements OnInit {

  _menuTitle: string;
  _saveRouteButtonText: string;
  _loadRouteButtonText: string;
  _newRouteButtonText: string;

  _routes: any[];
  _isSelected: boolean[];
  _selectedRoute: any;

  private parentSubscription = new Subscription;

  constructor(private communicationService: CommunicationServiceService) { }

  ngOnInit() {

    // Handling subscriptions
    this.getRoutesMapToMenu();

    this._menuTitle = 'Saved routes';
    this._saveRouteButtonText = 'Save route';
    this._loadRouteButtonText = 'Load route';
    this._newRouteButtonText = 'New route';

    this._routes = [];
    this._isSelected = [];
    this._selectedRoute = undefined;

  }

  handleSaveClick(event: any){
    // Sends a signal through the communication service to whomever may be listening
    this.communicationService.setListenToSaveRouteButton(true);
  }

  handleLoadClick(event: any){
    // Sends the selected route through the communication service to whomever may be listening
    if(this._selectedRoute !== undefined){
      this.communicationService.setRouteMenuToMap(this._selectedRoute);
    }
  }

  handleLNewClick(event: any){
    // Sends a signal through the communication service to whomever may be listening
    this.communicationService.setListenToNewRouteButton(true);
  }

  handleRouteClick(i: number){
   
    // If the route clicked was previously selected
    if(this._isSelected[i]) {

      this._selectedRoute = undefined;
      this._isSelected[i] = false;

    }
    else{

      this.clearSelection();
      this._selectedRoute = this._routes[i];
      this._isSelected[i] = true;

    }

  }

  clearSelection(){

    for(var i=0; i<this._isSelected.length; i++){
      this._isSelected[i] = false;
    }

  }


  getRoutesMapToMenu(){
    const subscription = this.communicationService.getRouteMapToMenu().subscribe(
      (route: any) => {
        this._routes.push(route);
        this._isSelected.push(false);
      }
    );

    this.parentSubscription.add(subscription);
  }

}
