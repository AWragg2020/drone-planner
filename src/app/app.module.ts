import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MatCardModule, MatButtonModule } from '@angular/material';
import { MapComponentComponent } from './map-component/map-component.component';
import { GeneralHeaderComponentComponent } from './general-header-component/general-header-component.component';
import { LeftMenuComponentComponent } from './left-menu-component/left-menu-component.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponentComponent,
    GeneralHeaderComponentComponent,
    LeftMenuComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    //Angular Material modules
    MatCardModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
