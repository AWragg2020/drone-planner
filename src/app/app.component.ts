import { Component } from '@angular/core';
import { Point } from './_models/point';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {

  title: string;

  ngOnInit(){

    this.title = "Drone Planner - Test by Adrian Wragg";

  }
  
}
