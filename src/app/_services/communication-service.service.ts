import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationServiceService {

  constructor() { }

  private saveRouteButtonClicked: Subject<any> = new Subject();
  private loadRouteButtonClicked: Subject<any> = new Subject();
  private newRouteButtonClicked: Subject<any> = new Subject();

  private routeMapToMenu: Subject<any> = new Subject();
  private routeMenuToMap: Subject<any> = new Subject();
  

  get listenToSaveRouteButton(): Observable<boolean>{
    return this.saveRouteButtonClicked.asObservable();
  }
  setListenToSaveRouteButton(event: any){
    this.saveRouteButtonClicked.next(event);
  }


  get listenToLoadRouteButton(): Observable<any>{
    return this.loadRouteButtonClicked.asObservable();
  }
  setListenToLoadRouteButton(event: any){
    this.loadRouteButtonClicked.next(event);
  }
  

  get listenToNewRouteButton(): Observable<any>{
    return this.newRouteButtonClicked.asObservable();
  }
  setListenToNewRouteButton(event: any){
    this.newRouteButtonClicked.next(event);
  }


  getRouteMapToMenu(): Observable<any>{
    return this.routeMapToMenu.asObservable();
  }
  setRouteMapToMenu(route: any){
    this.routeMapToMenu.next(route);
  }

  getRouteMenuToMap(): Observable<any>{
    return this.routeMenuToMap.asObservable();
  }
  setRouteMenuToMap(route: any){
    this.routeMenuToMap.next(route);
  }
}
