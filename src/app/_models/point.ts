export class Point{

    _xCoordiante: number;
    _yCoordinate: number;

    constructor(public x: number, public y: number) { 
        this._xCoordiante = x;
        this._yCoordinate = y;
    }

    public xCoordinate(): number{
        return this._xCoordiante;
    }

    public yCoordinate(): number{
        return this._yCoordinate;
    }
}